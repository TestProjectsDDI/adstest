<?php

use Illuminate\Http\Request;
use App\document;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Route::group(['namespace' => 'Api'], function () {
//     Route::group(['namespace' => 'Auth'], function () {
//         Route::post('v1/register', 'RegisterController@register');
//         Route::post('v1/login', 'LoginController@login')->name('login');;
//         Route::post('v1/logout', 'LogoutController')->middleware('auth:api');
//     });
// });
Route::group([
// 'middleware' => 'auth:api',
'prefix' => "v1",
], function () {
    Route::post('document', 'DataController@createOrPaginate')->name('Create');
    Route::get('document', 'DataController@createOrPaginate')->name('Paginate');
    Route::get('document/{id}', 'DataController@find');
    Route::patch('document/{id}', 'DataController@edit');
    Route::post('document/{id}/publish', 'DataController@publish');
});
