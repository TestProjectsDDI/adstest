## About
    Project created for test task for ADS. 
## Requirements
    Laravel >=5.8
    PHP >= 7.2
    MySql >= 8
    Composer
## Installing
    1. Clone repository.
    2. Setup apache2/nginx or serve server to /ads/public 
    3. Setup DB connection in .env (use utf8mb4_general_ci collation).
    4. Migrate, install passport (if you plannning to use passport auth.
    5. Done
## API endpoints and routes
    +--------+----------+-----------------------------------------+-----------------------------------+---------------------------------------------------------------------------+------------+
    | Domain | Method   | URI                                     | Name                              | Action                                                                    | Middleware |
    +--------+----------+-----------------------------------------+-----------------------------------+---------------------------------------------------------------------------+------------+
    |        | GET|HEAD | /                                       |                                   | Closure                                                                   | web        |
    |        | GET|HEAD | api/v1/document                         | Paginate                          | App\Http\Controllers\DataController@createOrPaginate                      | api        |
    |        | POST     | api/v1/document                         | Create                            | App\Http\Controllers\DataController@createOrPaginate                      | api        |
    |        | PATCH    | api/v1/document/{id}                    |                                   | App\Http\Controllers\DataController@edit                                  | api        |
    |        | GET|HEAD | api/v1/document/{id}                    |                                   | App\Http\Controllers\DataController@find                                  | api        |
    |        | POST     | api/v1/document/{id}/publish            |                                   | App\Http\Controllers\DataController@publish                               | api        |
    |        | POST     | oauth/authorize                         | passport.authorizations.approve   | Laravel\Passport\Http\Controllers\ApproveAuthorizationController@approve  | web,auth   |
    |        | GET|HEAD | oauth/authorize                         | passport.authorizations.authorize | Laravel\Passport\Http\Controllers\AuthorizationController@authorize       | web,auth   |
    |        | DELETE   | oauth/authorize                         | passport.authorizations.deny      | Laravel\Passport\Http\Controllers\DenyAuthorizationController@deny        | web,auth   |
    |        | POST     | oauth/clients                           | passport.clients.store            | Laravel\Passport\Http\Controllers\ClientController@store                  | web,auth   |
    |        | GET|HEAD | oauth/clients                           | passport.clients.index            | Laravel\Passport\Http\Controllers\ClientController@forUser                | web,auth   |
    |        | PUT      | oauth/clients/{client_id}               | passport.clients.update           | Laravel\Passport\Http\Controllers\ClientController@update                 | web,auth   |
    |        | DELETE   | oauth/clients/{client_id}               | passport.clients.destroy          | Laravel\Passport\Http\Controllers\ClientController@destroy                | web,auth   |
    |        | GET|HEAD | oauth/personal-access-tokens            | passport.personal.tokens.index    | Laravel\Passport\Http\Controllers\PersonalAccessTokenController@forUser   | web,auth   |
    |        | POST     | oauth/personal-access-tokens            | passport.personal.tokens.store    | Laravel\Passport\Http\Controllers\PersonalAccessTokenController@store     | web,auth   |
    |        | DELETE   | oauth/personal-access-tokens/{token_id} | passport.personal.tokens.destroy  | Laravel\Passport\Http\Controllers\PersonalAccessTokenController@destroy   | web,auth   |
    |        | GET|HEAD | oauth/scopes                            | passport.scopes.index             | Laravel\Passport\Http\Controllers\ScopeController@all                     | web,auth   |
    |        | POST     | oauth/token                             | passport.token                    | Laravel\Passport\Http\Controllers\AccessTokenController@issueToken        | throttle   |
    |        | POST     | oauth/token/refresh                     | passport.token.refresh            | Laravel\Passport\Http\Controllers\TransientTokenController@refresh        | web,auth   |
    |        | GET|HEAD | oauth/tokens                            | passport.tokens.index             | Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@forUser | web,auth   |
    |        | DELETE   | oauth/tokens/{token_id}                 | passport.tokens.destroy           | Laravel\Passport\Http\Controllers\AuthorizedAccessTokenController@destroy | web,auth   |
    +--------+----------+-----------------------------------------+-----------------------------------+---------------------------------------------------------------------------+------------+

#### OAUTH routes - scaffoldings for passport auth. Not used for time save.

## API docs
List of API requests:

    #### POST /api/v1/document - creating new empty document;
    #### GET /api/v1/document/{id} - getting document by id;
    #### POST /api/v1/document/{id}/publish - publish document by id;
    #### PATCH /api/v1/document/{id} - edit document by id;
    #### POST /api/v1/document/?page=1&perPage=2 - get documents by creating date with pagination.

### Examples:

    #### POST REQUEST: /api/v1/document/

    #### RESPONSE: 
    
    {
        "payload": "{}",
        "updated_at": "2020-09-23 12:00:34",
        "created_at": "2020-09-23 12:00:34",
        "id": 5
    }

    #### GET REQUEST: /api/v1/document/3

    #### RESPONSE:
    {
        "id": 3,
        "status": "draft",
        "payload": "{\n  \"actor\": \"The Fox\",\n  \"meta\":{\n    \"type\": \"quick\",\n    \"color\": \"brown\",\n  },\n  \"actions\":[{\n    \"action\": \"Jump Over\",\n    \"actor\": \"lazy dog\",\n  }],\n}",
        "created_at": "2020-09-23 10:12:53",
        "updated_at": "2020-09-23 10:12:53"
    }

    #### PATCH REQUEST: /api/v1/document/5 with $payload:
    {
      "actor": "The Fox",
      "meta":{
        "type": "quick",
        "color": "brown",
      },
      "actions":[{
        "action": "Jump Over",
        "actor": "lazy dog",
      }],
    }

    #### RESPONSE:
    {
        "id": 5,
        "status": "draft",
        "payload": "{\n  \"actor\": \"The Fox\",\n  \"meta\":{\n    \"type\": \"quick\",\n    \"color\": \"brown\",\n  },\n  \"actions\":[{\n    \"action\": \"Jump Over\",\n    \"actor\": \"lazy dog\",\n  }],\n}",
        "created_at": "2020-09-23 12:00:34",
        "updated_at": "2020-09-23 12:11:33"
    }

    #### POST REQUEST /api/v1/document/5/publish 

    #### RESPONSE:
    {
        "id": 5,
        "status": "published",
        "payload": "{\n  \"actor\": \"The Fox\",\n  \"meta\":{\n    \"type\": \"quick\",\n    \"color\": \"brown\",\n  },\n  \"actions\":[{\n    \"action\": \"Jump Over\",\n    \"actor\": \"lazy dog\",\n  }],\n}",
        "created_at": "2020-09-23 12:00:34",
        "updated_at": "2020-09-23 12:12:27"
    }

    #### GET REQUEST /api/v1/document/?page=1&perPage=2

    #### RESPONSE:
    {
        "current_page": 1,
        "data": [
            {
                "id": 5,
                "status": "published",
                "payload": "{\n  \"actor\": \"The Fox\",\n  \"meta\":{\n    \"type\": \"quick\",\n    \"color\": \"brown\",\n  },\n  \"actions\":[{\n    \"action\": \"Jump Over\",\n    \"actor\": \"lazy dog\",\n  }],\n}",
                "created_at": "2020-09-23 12:00:34",
                "updated_at": "2020-09-23 12:12:27"
            },
            {
                "id": 4,
                "status": "draft",
                "payload": "{\n  \"actor\": \"The Fox\",\n  \"meta\":{\n    \"type\": \"quick\",\n    \"color\": \"brown\",\n  },\n  \"actions\":[{\n    \"action\": \"Jump Over\",\n    \"actor\": \"lazy dog\",\n  }],\n}",
                "created_at": "2020-09-23 10:13:21",
                "updated_at": "2020-09-23 10:45:38"
            }
        ],
        "first_page_url": "http://localhost:8080/api/v1/document?page=1",
        "from": 1,
        "last_page": 3,
        "last_page_url": "http://localhost:8080/api/v1/document?page=3",
        "next_page_url": "http://localhost:8080/api/v1/document?page=2",
        "path": "http://localhost:8080/api/v1/document",
        "per_page": "2",
        "prev_page_url": null,
        "to": 2,
        "total": 5
    }
## Errors handling

    #### Document not found - 404;
    #### Edit published document - 400;
    #### Publish published document - 200;
    #### Edit with empty payload - 400.

## Methods docs

    ### DataContntroller:

    #### createOrPaginate - creating new document or returning collection of paginated documents (int $page - page number of paginate, int $perPage - number of documents on page);
    #### find - returns document by specified id (int $id - id of document to find);
    #### edit - updates payload of document by id and returns updated document (longtext $payload - new payload for document, int $id - id of updating document);
    #### publish - publishing of document by id (int $id - id of document to publish).

### Register/Login/Logout Controllers:

    #### Scaffolding methods to add passport auth. Not used for time save.

