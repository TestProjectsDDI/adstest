<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class document extends Model
{
  protected $fillable = [
  'id', 'status', 'payload', 'created_at', 'updated_at'
];
}
