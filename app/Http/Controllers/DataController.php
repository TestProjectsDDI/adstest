<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\document;
use Illuminate\Support\Facades\Input;

class DataController extends Controller
{
    public function createOrPaginate()
    {
        if (Input::get('page')&&Input::get('perPage')) {
            $docs=document::orderBy('created_at', 'desc')->paginate(Input::get('perPage'));
            return response()->json($docs, 200);
        } else {
            $doc=new document;
            $doc->payload=json_encode(json_decode("{}"));
            $doc->save();
            return response()->json($doc, 200);
        }
    }
    public function find($id)
    {
        $doc = document::find($id);
        return $doc ? response()->json($doc, 200) : abort(404);
    }
    public function edit(Request $request, $id)
    {
        $doc = document::find($id);
        if ($doc->status<>"published"&&$request->payload) {
            $doc->payload=$request->payload;
            $doc->save();
            return response()->json($doc, 200);
        } else {
            return response()->json("Error 400", 400);
        }
    }
    public function publish($id)
    {
        $doc= document::find($id);
        if ($doc->status<>"published") {
            $doc->status="published";
            $doc->save();
            return response()->json($doc, 200);
        } else {
            return response()->json("OK", 200);
        }
    }
}
