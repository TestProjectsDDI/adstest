require('./bootstrap');
require('bootstrap/js/src/index');
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap-vue/dist/bootstrap-vue.css'


window.Vue = require('vue');

//Imports use
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
//Comps reg glob
// Vue.component('first', ()=>import(/* webpackChunkName: "first" */ './components/first.vue'));

const app = new Vue({
    el: '#app'
});
